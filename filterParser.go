package filterParser

import (
	"fmt"
	"regexp"
	"strings"
)

// Reference: http://golangcookbook.com/chapters/strings/processing-complex/
func Parse(input string) (string, error) {
	var err error

	var fullMatches = regexp.MustCompile(`[|,]*\([a-zA-Z0-9_\[\],%|\-><=!'"\.\\]+\)`).FindAllStringSubmatch(input, -1)
	if fullMatches == nil {
		return "", fmt.Errorf("%s No matches found", "Level 1")
	}

	var conditionSlice []string
	for _, fullMatch := range fullMatches {
		var fullMatchStr = fullMatch[0]
		var fullMatchTemplate = `(%s)`
		if strings.HasPrefix(fullMatchStr, "|") {
			fullMatchTemplate = `OR (%s)`
		} else if strings.HasPrefix(fullMatchStr, ",") {
			fullMatchTemplate = `AND (%s)`
		}

		//fmt.Printf("Level 1 - Loop %d - %s\n", i, fmt.Sprintf(fullMatchTemplate, fullMatchStr))
		var fullMatchSlice []string
		var fullMatchRegexp = regexp.MustCompile(`[|,]*[a-zA-Z0-9_\-]+\[[!a-zA-Z0-9_\-<>=,%'"\.\\]+]`)
		var filterMatches = fullMatchRegexp.FindAllStringSubmatch(fullMatchStr, -1)
		if filterMatches == nil {
			return "", fmt.Errorf("%s No matches found", "Level 2")
		}

		for _, filterMatch := range filterMatches {
			var filterMatchStr = filterMatch[0]
			var filterMatchTemplate = `(%s)`
			if strings.HasPrefix(filterMatchStr, "|") {
				filterMatchTemplate = `OR (%s)`
			} else if strings.HasPrefix(filterMatchStr, ",") {
				filterMatchTemplate = `AND (%s)`
			}

			//fmt.Printf("Level 2 - Loop %d - %s \n", i, fmt.Sprintf(filterMatchTemplate, filterMatchStr))
			var filterContentRegexp = regexp.MustCompile(`\[.+\]`)
			var fieldName = filterContentRegexp.Split(filterMatchStr, -1)[0]
			if strings.HasPrefix(fieldName, "|") || strings.HasPrefix(fieldName, ",") {
				fieldName = fieldName[1:]
			}

			var conditionMatchRegexp = regexp.MustCompile(`\[(.+)\]`)
			var conditionMatches = conditionMatchRegexp.FindAllStringSubmatch(filterMatchStr, -1)
			if conditionMatches == nil {
				return "", fmt.Errorf("%s No matches found", "Level 3")
			}

			var conditionOutput string
			var conditionContent string
			for _, condition := range conditionMatches {
				//fmt.Printf("Level 3 - Loop %d\n", i)
				if len(condition) >= 2 {
					var condPrefixRegexp = regexp.MustCompile(`[!><].+`)
					var conditionMatchStr = condition[1]
					if condPrefixRegexp.Match([]byte(conditionMatchStr)) {
						var notFilter = strings.HasPrefix(conditionMatchStr, "!")
						var likeFilter = strings.Contains(conditionMatchStr, "%")

						if likeFilter {
							conditionContent = getLikeCondition(fieldName, conditionMatchStr)
							conditionOutput = fmt.Sprintf(filterMatchTemplate, conditionContent)
							//fmt.Println("Level 3 LIKE|NOT LIKE filter: " + conditionOutput)
						} else if notFilter && !likeFilter {
							conditionContent = getEqOrInCondition(fieldName, conditionMatchStr)
							conditionOutput = fmt.Sprintf(filterMatchTemplate, conditionContent)
							//fmt.Println("132: Level 3 NOT EQ|IN filter: " + conditionOutput)
						} else {
							var eqRegexp = regexp.MustCompile(`^[>|<][=]*.+$`)
							var isEq = eqRegexp.Match([]byte(conditionMatchStr))
							if isEq {
								conditionMatchStr, err = getGreatOrLessCondition(fieldName, conditionMatchStr)
								if err != nil {
									return "", err
								}
								conditionOutput = fmt.Sprintf(filterMatchTemplate, conditionMatchStr)
							} else {
								conditionContent = getEqOrInCondition(fieldName, conditionMatchStr)
								conditionOutput = fmt.Sprintf(filterMatchTemplate, conditionContent)
								//fmt.Println("178: Level 3 EQ|IN (conditionContent) filter: " + conditionOutput)
							}
						}
					} else {
						if strings.Contains(conditionMatchStr, "%") {
							conditionContent = getLikeCondition(fieldName, conditionMatchStr)
							conditionOutput = fmt.Sprintf(filterMatchTemplate, conditionContent)
							//fmt.Println("128: Level 3 LIKE filter: " + conditionOutput)
						} else {
							conditionContent = getEqOrInCondition(fieldName, conditionMatchStr)
							conditionOutput = fmt.Sprintf(filterMatchTemplate, conditionContent)
							//fmt.Println("224: Level 3 EQ|IN filter: " + conditionOutput)
						}
					}
					fullMatchSlice = append(fullMatchSlice, conditionOutput)
				} else {
					return "", fmt.Errorf("%s filter length < 2", "Level 3")
				}
			}
		}
		conditionSlice = append(conditionSlice, fmt.Sprintf(fullMatchTemplate, strings.Join(fullMatchSlice, " ")))
		fullMatchSlice = nil
	}
	//fmt.Printf("Parsed filter - %s\n", strings.Join(conditionSlice, " "))
	return strings.Join(conditionSlice, " "), nil
}

func getEqOrInCondition(field string, condition string) string {
	//var err error
	var conditionStr = condition
	var isNotCondition = strings.HasPrefix(conditionStr, "!")
	if isNotCondition {
		conditionStr = conditionStr[1:]
	}
	conditionStr = strings.Replace(conditionStr, `\,`, `\t`, -1)
	conditionStr = strings.Replace(conditionStr, `"`, `\"`, -1)

	var strRegexp = regexp.MustCompile(`([^\d^,])`)
	var isString = strRegexp.Match([]byte(conditionStr))

	var splitRegexp = regexp.MustCompile(`,`)
	var conditionSlice = splitRegexp.Split(conditionStr, -1)

	var template string
	var operator string
	if len(conditionSlice) > 1 { // IN condition
		template = `%s %s (%s)`
		var sep = ","
		operator = "IN"
		if isString {
			sep = `","`
			template = `%s %s ("%s")`
		}
		if isNotCondition {
			operator = "NOT IN"
		}
		conditionStr = strings.Join(conditionSlice, sep)
		conditionStr = strings.Replace(conditionStr, `\t`, `\,`, -1)
	} else { // Equal condition
		template = `%s%s%s`
		operator = "="
		conditionStr = conditionSlice[0]
		conditionStr = strings.Replace(conditionStr, `"`, `\"`, -1)
		if isString {
			template = `%s%s"%s"`
		}
		if isNotCondition {
			operator = "!="
		}
	}
	return fmt.Sprintf(template, field, operator, conditionStr)
}

func getGreatOrLessCondition(field string, condition string) (string, error) {
	var eqRegexpr = regexp.MustCompile(`^[>|<]=.+$`)
	// true: check first 2 characters
	var isEq = eqRegexpr.Match([]byte(condition))
	var conditionStr = condition
	var operator string
	if isEq {
		operator = condition[0:2]
		conditionStr = condition[2:]
	} else {
		operator = condition[0:1]
		conditionStr = condition[1:]
	}

	var isGt = strings.HasPrefix(operator, ">")
	var isLt = strings.HasPrefix(operator, "<")

	if !isGt && !isLt {
		return "", fmt.Errorf("invalid filter operator %s [%s]", operator, conditionStr)
	}
	var hasStringRegexp = regexp.MustCompile(`((\D)|([x|X]))`)

	var template = `%s%s%s`
	if hasStringRegexp.Match([]byte(conditionStr)) {
		return "", fmt.Errorf("invalid filter characters %s", conditionStr)
	}
	return fmt.Sprintf(template, field, operator, conditionStr), nil
}

func getLikeCondition(field string, condition string) string {
	var likeRegexp = regexp.MustCompile(`^!.+$`)
	var isNotLike = likeRegexp.Match([]byte(condition))
	var template = `%s LIKE "%s"`
	if isNotLike {
		template = `%s NOT LIKE "%s"`
		condition = condition[1:]
	}
	condition = strings.Replace(condition, `"`, `\"`, -1)
	return fmt.Sprintf(template, field, condition)
}
